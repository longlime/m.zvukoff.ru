(function( window, $ ) {

	var
		// Optimization
		document = window.document,
		_proto = "prototype",

		// DOM
		$window = $(window),
		$body = $(document.body),
		b_content = $(".content"),
		b_footer = $("#screen .footer"),
		b_menu = $("#menu"),
		b_playlist = $(".content .playlists-list ");

	$.fn.draggable = function( totalWidth ) {
		var offset = null;

		if ( !totalWidth ) {
			totalWidth = this.width();
		}

		var parent = this.parent();

		var max = 0;

		var start = function( e ) {
			var orig = e.originalEvent;
			var pos = $(this).position();
			offset = {
				x: orig.changedTouches[0].pageX - pos.left,
				// y: orig.changedTouches[0].pageY - pos.top
			};
		};
		var moveMe = function(e) {
			e.preventDefault();
			var orig = e.originalEvent;
			var value = orig.changedTouches[0].pageX - offset.x;

			if ( value > 0 ) value = 0;
			if ( value < -max ) value = -max;

			$(this).css({
				left: value
				// top: orig.changedTouches[0].pageY - offset.y,
			});
		};

		var setMax = function() {
			var parentWidth = parent.width();
			max = ( totalWidth > parentWidth ) ? totalWidth - parentWidth : 0;
		};

		setMax();

		$window.on( "resize", setMax );

		this.on( "touchstart", start );
		this.on( "touchmove", moveMe );
	};

	var TabsManager,
		TabsBlock = $(".tabs"),
		Tabs = (function() {
			function Tabs() {
				var that = this;

				that.block = TabsBlock;
				that.list = TabsBlock.find(".tabs-links");
				that.items = that.list.find(".tabs-links-item");

				// that.setWidth();
				that.addScrolling();
				that.addHandlers();
			}

			Tabs[ _proto ].showBlock = function( block ) {
				block
					.addClass("tabs-block_active")
					.siblings(".tabs-block").removeClass("tabs-block_active");
			};

			Tabs[ _proto ].addHandlers = function() {
				var that = this;

				var linkClicked = function() {
					var _this = this;

					$( _this ).parent()
						.addClass("tabs-links-item_active")
						.siblings(".tabs-links-item").removeClass("tabs-links-item_active");

					that.showBlock( $( _this.hash ) );
					return false;
				};

				that.items.find("a").on( "click", linkClicked );
			};

			Tabs[ _proto ].addScrolling = function() {
				var that = this;

				that.list.draggable( that.getTotalWidth() );
			};

			Tabs[ _proto ].getTotalWidth = function() {
				var totalWidth = 0;

				this.items.each(function( i, item ) {
					var $item = $(item);
					totalWidth += $item.width() + parseInt( $item.css("paddingLeft") ) + parseInt( $item.css("paddingRight") );
				});

				return totalWidth;
			};

			return Tabs;
		})();

	if ( TabsBlock.length )
		TabsManager = new Tabs();

	var TracksManager,
		TracksBlock = $(".tracks"),
		Tracks = (function() {
			function Tracks() {
				var that = this;

				that.addShowDescriptionHandler();
			}

			Tracks[ _proto ].addShowDescriptionHandler = function() {
				var clicked = function() {
					$(this).closest(".tracks-item")
						.toggleClass("tracks-item_opened")
						.siblings().removeClass("tracks-item_opened");
				};
				$(".tracks-button_showDescription").on("click", clicked);
			};

			return Tracks;
		})();

	// if ( TracksBlock.length )
		TracksManager = new Tracks();

	var MenuManager,
		Menu = (function() {
			function Menu() {
				var that = this;

				that.handlerBlock = $(".header-menu");
				that.addHandlers();
			}

			Menu[ _proto ].toggleMenu = function() {
				$body.toggleClass("show-menu").removeClass("show-menu-addtrack");
				return false;
			};
			Menu[ _proto ].toggleAddtrack = function() {
				$body.toggleClass("show-menu-addtrack");
				return false;
			};
			var timer;
			Menu[ _proto ].easeMenu = function(delay) {
				delay = delay || 0;

				setTimeout(function() {
					b_menu.removeClass("menu_ease").addClass("menu_ease");
					var $menu = b_menu;
					var width = 272;
					var dur = 200;

					clearTimeout(timer);

					if ($body.hasClass("screen-full")) {

					$menu.css("marginLeft", 44);
					timer = setTimeout(function() {
						$menu.css("marginLeft", 4);
					}, dur);
					timer = setTimeout(function() {
						$menu.css("marginLeft", 44);
					}, 2*dur);
					timer = setTimeout(function() {
						$menu.css("marginLeft", 0);
					}, 3*dur);
					timer = setTimeout(function() {
						$menu.removeClass("menu_ease");
					}, 4*dur);

					} else {


					$menu.css("marginLeft",  44);
					timer = setTimeout(function() {
						$menu.css("marginLeft",  4);
					}, dur);
					timer = setTimeout(function() {
						$menu.css("marginLeft",  44);
					}, 2*dur);
					timer = setTimeout(function() {
						$menu.css("marginLeft", 0);
					}, 3*dur);

					}
				}, delay);

				return false;
			};

			Menu[ _proto ].addHandlers = function() {
				var that = this;

				if ($body.hasClass("anonym")) {
					that.handlerBlock.on("click", that.easeMenu);
				} else {
					that.handlerBlock.on("click", that.toggleMenu);
				}
				b_menu.find(".icon-addtrack").on("click", that.toggleAddtrack);
			};

			return Menu;
		})();

		MenuManager = new Menu();

		// DEBUG
		// MenuManager.toggleMenu();

	var SearchManager,
		Search = (function() {
			function Search() {
				var that = this;

				that.handler = $(".header-search");
				that.closeButton = $(".search-close");
				that.b_input = $(".search-input input");
				that.addHandler();
			}

			Search[ _proto ].toggleSearch = function() {
				var that = this;

				if ( $body.hasClass("show-search") ) {
					that.hide();
				} else {
					that.show();
				}
			};

			Search[ _proto ].show = function() {
				$body.addClass("show-search");
				this.b_input.focus();
			};

			Search[ _proto ].hide = function() {
				$body.removeClass("show-search");
				this.b_input.blur();
			};

			Search[ _proto ].addHandler = function() {
				var that = this;

				that.handler.on("click", that.toggleSearch.bind( that ));
				that.closeButton.on("click", that.hide.bind( that ));
			};

			return Search;
		})();

		SearchManager = new Search();

		// DEBUG
		// SearchManager.toggleSearch();

	var SongshareManager,
		SongshareBlock = $(".song-sharing"),
		Songshare = (function() {
			function Songshare() {
				var that = this;

				that.handler = SongshareBlock.find(".song-sharing-button");
				that.addHandlers();
			}

			Songshare[ _proto ].toggleBlock = function() {
				SongshareBlock.toggleClass("song-sharing_open");
			};

			Songshare[ _proto ].addHandlers = function() {
				var that = this;

				that.handler.on("click", that.toggleBlock.bind(that));
			};

			return Songshare;
		})();

	if ( SongshareBlock.length )
		SongshareManager = new Songshare();

	var BlockManager,
		Block = (function() {
			function Block() {
				var that = this;

				that.handlers = $(".b-handler");
				that.addHandlers();
			}

			Block[ _proto ].addHandlers = function() {
				var that = this;

				var clicked = function() {
					var $this = $(this);

					$this.closest(".b").toggleClass("b_open");
				};

				that.handlers.on("click", clicked);
			};

			return Block;
		})();

		BlockManager = new Block();

	var PlaylistBlock = b_playlist,
		PlaylistManager,
		Playlist = (function() {
			function Playlist() {
				var that = this;

				that.b_handler_1 = $(".playlists-button_showDescription");
				that.b_handler_2 = $(".icon-rename");
				that.b_handler_3 = $(".playlists-handler-done");
				that.b_handler_4 = $(".playlists-handler-done, .icon-done");
				that.addHandlers();
			}

			Playlist[ _proto ].openDescription = function() {
				var $this = $(this);

				$this.closest(".playlists-item").toggleClass("playlists-item_opened");

				return false;
			};

			Playlist[ _proto ].openRename = function() {
				var $this = $(this);

				$this.closest(".playlists-item")
					.toggleClass("playlists-controls_rename")
					.find("INPUT").focus();

				return false;
			};

			Playlist[ _proto ].saveNewname = function() {
				var item = $(this).closest(".playlists-item");
				var value = item.find("INPUT").val();

				item.find(".playlists-title").html(value);

				item.removeClass("playlists-controls_rename playlists-item_opened");

				return false;
			};

			Playlist[ _proto ].save = function() {
				var item = $(this).closest(".playlists-item");

				if ( item.hasClass("checked") ) {
					item.removeClass("checked")
						.find(".input-checkbox").removeClass("input-checkbox_checked")
						.siblings("input").attr("checked", false);
				} else {
					var value = item.find(".playlists-controls-rename").val();

					item.find(".playlists-title").html(value);

					item.removeClass("playlists-controls_rename playlists-item_opened");
				}

				return false;
			};

			Playlist[ _proto ].addHandlers = function() {
				var that = this;

				$("#screen .content")
					.on("click", ".playlists-button_showDescription", that.openDescription)
					.on("click", ".icon-rename", that.openRename)
					.on("click", ".playlists-handler-done, .icon-done", that.save);
			};

			return Playlist;
		})();

	// if ( PlaylistBlock.size() )
		PlaylistManager = new Playlist();

	var LoginpanelManager,
		Loginpanel = (function() {
			function Loginpanel() {
				var that = this;

				that.handler = b_menu.find(".icon-loginpanel");
				that.addHandlers();
				that.styleCheckbox();
				that.enableTabs();
			}

			Loginpanel[ _proto ].togglePanel = function() {
				$body.toggleClass("show-menu-login");

				return false;
			};

			Loginpanel[ _proto ].addHandlers = function() {
				var that = this;

				that.handler.on("click", that.togglePanel);
			};

			Loginpanel[ _proto ].enableTabs = function() {
				var showTab = function( $this ) {
					var i = $this.index() + 1;
					$(".loginpanel-" + i).show().siblings(".lp-tabs-cont").hide();
					$this.addClass("loginpanel-tabs-item_active")
						.siblings(".tabs-links-item").removeClass("loginpanel-tabs-item_active");
				};

				var clicked = function() {
					showTab($(this));
				};

				$(".loginpanel-tabs-item").on("click", clicked).first().click();
			};

			Loginpanel[ _proto ].styleCheckbox = function() {
				var that = this;

				$(".menu-loginpanel").find("input[type=checkbox]").each(function(i, input) {
					var $input = $(input);

					var html = "<div class='input-checkbox" + ($input.is(":checked") ? " input-checkbox_checked" : "") + "'></div>"

					$input.hide();
					$input.after(html);

					var check = function(elem) {
						$input.attr("checked", "checked");
						elem.addClass("input-checkbox_checked");
					};

					var uncheck = function(elem) {
						$input.attr("checked", false);
						elem.removeClass("input-checkbox_checked");
					};

					var inputclicked = function() {
						var $this = $(this);
						if ($this.hasClass("input-checkbox_checked")) {
							uncheck($this);
						} else {
							check($this);
						}
					};

					$input.next().on("click", inputclicked);
				});
			};

			return Loginpanel;
		})();

			LoginpanelManager = new Loginpanel();

			// DEBUG
			// LoginpanelManager.togglePanel();

	var PlayerManager,
		Player = (function() {
			var timer;
			var pause = 5000;

			function Player() {
				var that = this;

				that.showCurrentTrack();
				that.addHandlers();
			}

			Player[ _proto ].play = function() {
				clearTimeout(timer);

				$(".musicControl").show();
				$(".musicInfo").show();

				timer = setTimeout(function() {
					// DEBUG
					// $(".musicControl").hide();
					windowResize();
				}, pause);

				windowResize();
			};

			Player[ _proto ].pause = function() {
				clearTimeout(timer);
				$(".musicControl").show();
				$(".musicInfo").show();

				windowResize();
			};

			Player[ _proto ].pauseTrack = function(that) {
				// this - handler
				// that - Player
				$(this).addClass("track-pause_next");
			};

			Player[ _proto ].continueTrack = function(that) {
				// this - handler
				// that - Player
				$(this).removeClass("track-pause_next");
			};

			Player[ _proto ].addHandlers = function() {
				var that = this;

				var clicked = function() {
					that.play();
					return false;
				};

				var clickedSe = function() {
					that.pause();
					return false;
				};

				$(".content .tracks-name").on("click", clicked);
				$(".footer .tracks-name").on("click", clickedSe);

				var pauseTrack = function() {
					that.pauseTrack.call(this, that);
					return false;
				};

				var continueTrack = function() {
					that.continueTrack.call(this, that);
					return false;
				};

				$(".footer")
					.on("click", ".track-pause", pauseTrack)
					.on("click", ".track-pause_next", continueTrack);
			};

			Player[ _proto ].showCurrentTrack = function() {
				var clicked = function() {
					$(this).closest(".musicInfo").toggleClass("musicInfo_opened");
				};

				b_footer.find(".tracks-button_showDescription")
					.on( "click", clicked );
			};

			return Player;
		})();

	// ---

	var showNewcommentPlace = function() {
		$(".song-comments-add").hide();
		$(".song-comments-newtext").show().find("textarea").focus();
		$(".song-comments-button").show();
		return false;
	};

	var hideNewcommentPlace = function() {
		$(".song-comments-add").show();
		$(".song-comments-newtext").hide().find("textarea").blur();
		$(".song-comments-button").hide();
		return false;
	};

	var inputStyle = function($input, cllb) {
		var html = "<div class='input-checkbox" + ($input.is(":checked") ? " input-checkbox_checked" : "") + "'></div>"

		$input.hide();
		$input.after(html);

		var check = function(elem) {
			$input.attr("checked", "checked");
			elem.addClass("input-checkbox_checked");
			cllb(elem) || 0;
		};

		var uncheck = function(elem) {
			$input.attr("checked", false);
			elem.removeClass("input-checkbox_checked");
			cllb(elem) || 0;
		};

		var inputclicked = function() {
			var $this = $(this);
			if ($this.hasClass("input-checkbox_checked")) {
				uncheck($this);
			} else {
				check($this);
			}
		};

		$input.next().on("click", inputclicked);
	};

	var toggleTracksSave = function() {
		var $this = $(this);

		$this.toggleClass("icon-cross");
		$body.toggleClass("show-m-playlists").removeClass("show-m-playlists_song");

		return false;
	};

	var TracksSaveChange = function(elem) {
		elem.closest(".playlists-item").toggleClass("checked");
	};

	$(".footer .tracks-save").on("click", toggleTracksSave);
	$(".song-comments-add").on("click", showNewcommentPlace);
	$(".song-comments-button I").on("click", hideNewcommentPlace);

	hideNewcommentPlace();

	// $body.addClass("show-m-playlists");

	$(".m-playlists").find("input[type=checkbox]").each(function(i, input) {
		inputStyle($(input), TracksSaveChange);
	});

	$(".radio-down").on("click", function() {
		// b_content.scrollTop(b_content.height());
		$(this).toggleClass("radio-down-other");
		return false;
	});

	var opened = false;
	var $songAddition = $(".song-addition");
	var $songAddition_html = $songAddition.html();
	$(".song-control-save").on("click", function() {
		$body.toggleClass("show-m-playlists_song");
		return false;
	});

	// Add new playlist
	var playlist_b = $("#screen .content .playlists-list");
	$(".handler-newplaylist").on("click", function() {
		var newitem_html = "";

		newitem_html += '<li class="playlists-item">';
		newitem_html += '<div class="playlists-title"></div>';
		newitem_html += '<div class="playlists-count">Песен: 0</div>';
		newitem_html += '<div class="playlists-controls"><input type="text" class="playlists-controls-rename"><div class="icon icon-cross"></div><div class="icon icon-rename"></div><div class="icon icon-done"></div></div><div class="tracks-button_showDescription playlists-button_showDescription"></div>';
		newitem_html += '</li>';

		playlist_b.prepend( newitem_html );

		var newitem = playlist_b.find(".playlists-item:eq(0)");

		newitem
			.find(".playlists-button_showDescription").click().end()
			.find(".icon-rename").click();
	});

	// Change repeat mode
	$(".tracks-reload").on("click", function() {
		$(this).toggleClass("tracks-reload_next");
		return false;
	});

	// ---

	var b_playlists = $(".m-playlists");
	var last = false;
	var windowResize = function() {
		var windowHeight = $window.height();
		var windowWidth = $window.width();
		var delta_footer = b_footer.length ? 88 : 0;
		if ( $(".screen-small .footer .musicControl").css("display") == "none" ) {
			delta_footer -= 44;
		}
		if ( $(".footer .musicInfo").css("display") == "none" ) {
			delta_footer -= 44;
		}
		if ( $body.hasClass("screen-full") ) {
			delta_footer -= 44;
		}

		b_content.height( windowHeight - 44 - delta_footer );

		b_playlists.height( windowHeight - 44 - delta_footer);
		if ( $(".songLine").size() ) {
			b_playlists.height(b_playlists.height() - 88);
		}
		$(".loginpanel-2 FORM").css( "minHeight", windowHeight - 167 );

		$("#menu").find(".content").height( windowHeight - 88 );
		$(".search-list UL").css( "maxHeight", windowHeight - 88 );

		if (windowWidth <= 768) {
			$body.addClass("screen-small").removeClass("screen-full");
			if (last != "small") {
				MenuManager.easeMenu();
				last = "small";
			}
		} else {
			$body.removeClass("screen-small").addClass("screen-full");
			$(".musicInfo").removeClass("musicInfo_opened");
			if (last != "full") {
				MenuManager.easeMenu();
				last = "full";
			}
		}
	};
	windowResize();

	$window.on( "resize", windowResize );
	$('input[placeholder], textarea[placeholder]').placeholder();
	$window.on( "orientationchange", windowResize );

	// ---

	PlayerManager = new Player();

	// DEBUG
	PlayerManager.play();

})( window, jQuery );
